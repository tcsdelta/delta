// Initialise Express

var fs = require('fs');

// initialise mongodb connection
var mongodb = require('./mongo.js');
var express = require('express');
var app = express();
var http = require('http');
var bodyParser = require('body-parser');
var winston = require('winston');


app.use(bodyParser.json({
    limit: 10 * 1024 * 1024
}));
app.use(bodyParser.raw({
    limit: 10 * 1024 * 1024
}));
app.use(bodyParser.text({
    limit: 10 * 1024 * 1024
}));
app.use(bodyParser.urlencoded({
    limit: 10 * 1024 * 1024,
    extended: true
}));


var PORT = 9004;

// Initialize mongodb
mongodb.init(init_appserver, function(err) {
    if (err) {
        winston.error("FATAL: MongoDb database cannot be initialized. Shutting down!!");
        process.exit();
    }
});



var registerRoutes = function (app) {
    var routeDir = __dirname + '/routes';
    fs.readdirSync(routeDir).forEach(function (filename) {
        if (~filename.indexOf('.js')) {
            var routes = require(routeDir + '/' + filename);
            routes.configure(app);
        }
    });
};

// callback for starting the node server
function init_appserver() {
    try {
      http.createServer(app).listen(PORT, function() {
          winston.info("Server started on port " + PORT);
          registerRoutes(app);
      });
    } catch (ex) {
        winston.error("Exception Occurred " + ex);
    }
}

// init_appserver();
