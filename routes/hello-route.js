var helloService = require('../service/hello-service.js');

exports.configure = function (app, passport) {
    app.get('/hello', helloService.hello);
};
